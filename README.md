# Federation data

This repository contains the data about federation and blocking between
fediverse instances.

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

## Instance data

Each instance entry should contain information in the following format:

```json
{
  "name": "example.inst",
  "url": "https://example.inst",
  "admin": "@admin@example.inst",
  "server": "mastodon",
  "known-servers": [
    "example-2.inst",
    "example-3.inst"
  ],
  "filter": [
    "bad-inst.example",
    "dead-server.example"
  ],
  "silence": [
    "bad-inst.example",
    "dead-server.example"
  ],
  "suspend": [
    "bad-inst.example",
    "dead-server.example"
  ],
  "activity": 5,
  "languages": ["en", "es", "fr"],
  "tags": ["general", "humor", "technology"],
  "open": true,
  "etc": [
    "This instance does not allow political posts",
    "This instance does not allow foo.bar as mail provider"
  ]
}
```

The fields `filter`, `silence`, and `suspend` are named after 3 mode of
moderation on Mastodon:

- Filter: Media will not be propagated from the filtered instance
- Silence: Posts will be hidden from public timeline, unless you're following
    the user
- suspend: There is no interaction at all with the servers

The `languages` field is for allowed languages, using 2-letter as specified in
ISO 639-1.  If the language is not specified in the standard (for example, it's
an Esperanto instance), use `xx` or `xx:<language name>`.  If any language is
allowed, use `*` as the only member in the field.

The field `open` is for open registration.

The field `activity` rates the activity on a scale 1-5:

- 0: no data available
- 1: not active (not every day)
- 2: low active (a few posts a day)
- 3: relatively active (more than 10 posts a day)
- 4: very active (more than 25 posts a day)
- 5: highly active (new posts almost any time a day)

The field `etc` and `tags` can be useful for people who are choosing instance.

The `known-servers` is for servers that are known to be in a federation with
the subject instance.  This is expected not to be a comprehensive list.  Please
limit this to be below 25 and keep the list to be the instances with most
interaction, ignoring bot instances.

## Software data

For server applications:

```json
{
  "name": "mastodon",
  "website": "https://joinmastodon.org",
  "repo": "https://github.com/tootsuite/mastodon",
  "protocols": ["activitypub"],
  "latest": "3.4.1",
  "languages": ["ruby", "js"]
}
```

For client applications:

```json
{
  "name": "Tusky",
  "repo": "https://github.com/tuskyapp/Tusky",
  "platform": "mobile/android",
  "servers": ["mastodon", "pleroma"],
  "latest": "15.1",
  "languages": ["kotlin", "java"]
}
```

The field `languages` is for implementation language, which is optional and
might be useful for potential contributors.  The list items should be sorted
decreasingly by the percentage if possible.

Some common fields for `platform` field:

- `web`: Web client
- `term`: TUI or CLI client
- `desktop`: Desktop or laptop client
  - `desktop/linux`: used on a Linux platform, whether it's with GNU or not
  - `desktop/mac`: used on MacOS
  - `desktop/windows`: used on Windows
- `mobile`: Mobile client
  - `mobile/android`: used on Android or derivative
  - `mobile/ios`: used on iOS
